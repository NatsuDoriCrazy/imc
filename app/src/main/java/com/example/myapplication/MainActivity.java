package com.example.myapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    EditText txtpeso;
    EditText txtaltura;

    Button btnborrar;
    Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtpeso = findViewById(R.id.txtpeso);
        txtaltura = findViewById(R.id.txtaltura);
        btnborrar= (Button) findViewById(R.id.btnborrar);
        btnSalir= (Button) findViewById(R.id.btnSalir);


        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btnborrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtpeso.setText("");
                txtaltura.setText("");

            }
        });
    }
    public void imc(View vista){
        String pesotexto = txtpeso.getText().toString();
        String alturatexto = txtaltura.getText().toString();
        if (pesotexto.equals("") || alturatexto.equals("")) {
            Toast.makeText(getApplicationContext(), "Por favor rellene los datos",Toast.LENGTH_LONG).show();
        }else{
            float peso = Integer.parseInt(pesotexto);
            float altura = Integer.parseInt(alturatexto);
            altura= altura/100;
            //Calcular
            float imc=peso/(float) Math.pow(altura,2);
            //Resultado a texto
            String imctexto = String.format("%.1f",imc);

setContentView(R.layout.activity_main);
            TextView textView = (TextView) findViewById(R.id.mostrarimc);
                    textView.setText("Tu indice de masa corporal es " + imctexto);
        }
    }

}